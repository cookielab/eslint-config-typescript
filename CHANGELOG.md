# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
This change log adheres to standards from [Keep a CHANGELOG](http://keepachangelog.com).

## [7.8.0] 2022-03-17
### Changed
- update dependencies
- Fix ESlint peer dependency version and other
- Turned on rule `no-unused-private-class-members`
- Turned on rule `@typescript-eslint/consistent-type-exports`
- Turned on rule `@typescript-eslint/no-meaningless-void-operator`
- Turned on rule `@typescript-eslint/no-non-null-asserted-nullish-coalescing`
- Turned on rule `@typescript-eslint/no-redundant-type-constituents`
- Turned on rule `@typescript-eslint/no-restricted-imports`
- Turned on rule `@typescript-eslint/no-throw-literal`
- Turned on rule `@typescript-eslint/no-unsafe-argument`
- Turned on rule `@typescript-eslint/no-useless-empty-export`
- Turned on rule `@typescript-eslint/object-curly-spacing`
- Turned on rule `@typescript-eslint/padding-line-between-statements`
- Turned on rule `@typescript-eslint/prefer-literal-enum-member`
- Turned on rule `@typescript-eslint/prefer-return-this-type`
- Turned on rule `@typescript-eslint/sort-type-union-intersection-members`
- Turned on rule `@typescript-eslint/space-before-blocks`
- Turned on rule `@typescript-eslint/non-nullable-type-assertion-style`

## [7.7.1] 2020-12-10
### Fixed
- Fix ESlint peer dependency version

## [7.7.0] 2020-12-10
### Changed
- update dependencies
- Turned on rule `no-nonoctal-decimal-escape`
- Turned on rule `no-unsafe-optional-chaining`
- Allow UPPER_CASE variable name in global scopes only

## [7.6.0] 2020-11-12
### Changed
- update dependencies
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/no-confusing-void-expression`

## [7.5.0] 2020-10-29
### Changed
- update dependencies
- Disable usage of `{}` and `[]` types by `@typescript-eslint/ban-types`
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/no-unnecessary-type-constraint`
    - `@typescript-eslint/space-infix-ops`

## [7.4.0] 2020-10-08
### Changed
- update dependencies
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/consistent-indexed-object-style`
    - `@typescript-eslint/no-duplicate-imports`

## [7.3.0] 2020-09-22
### Changed
- update dependencies
- Turned off rule `no-undef` to prevent false positives that are already checked by TypeScript itself (https://github.com/typescript-eslint/typescript-eslint/issues/2528#issuecomment-689369395)
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/comma-dangle`

### Fixed
- Fix having some rules used from TypeScript and from plain ESLint as well

## [7.2.1] 2020-09-16
### Changed
- update dependencies

## [7.2.0] 2020-09-14
### Changed
- update dependencies
- Used option `ignoreFunctionTypeParameterNameValueShadow` for `@typescript-eslint/no-shadow` rule
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/no-loop-func`

## [7.1.0] 2020-09-04
### Changed
- update dependencies
- Used option `ignoreTypeReferences` for `no-use-before-define` rule
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/consistent-type-imports`
    - `@typescript-eslint/no-redeclare`
    - `@typescript-eslint/no-shadow`

## [7.0.0] 2020-08-31
### Changed
- update dependencies
- update minimal TypeScript major version
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/no-implicit-any-catch`

## [6.3.0] 2020-08-20
### Changed
- update dependencies
- Used option `allowFunctionParams` for `no-underscore-dangle` rule
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/prefer-enum-initializers`

## [6.2.0] 2020-07-13
### Added
- add test for detection of extra overriden rules used in tests

### Changed
- update dependencies
- Used option `ignoreGlobals` for `camelcase` rule
- Used option `disallowRedundantWrapping` for `prefer-regex-literals` rule
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/prefer-literal-enum-member`

## [6.1.0] 2020-06-26
### Changed
- Turned on rules from native ESlint
    - `no-promise-executor-return`
    - `no-unreachable-loop`
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/no-loss-of-precision`

## [6.0.0] 2020-06-17
Updated ESlint and Typescript ESlint packages to the newest major versions resulting in changes below.

### Changed

- Used option `checkForEach` for `array-callback-return` rule
- Used option `offsetTernaryExpressions` for `indent` and `@typescript-eslint/indent` rules
- Used option `enforceForLogicalOperands` for `no-extra-boolean-cast` rule
- Used option `allowAsStatement` for `no-void` rule
- Turned on rules from native ESlint
    - `default-case-last`
    - `no-loss-of-precision`
    - `no-useless-backreference`
- Require description for allowed TS comments
- Used new options for `@typescript-eslint/strict-boolean-expressions` rule
- Turned on rules from Typescript ESlint
    - `@typescript-eslint/ban-tslint-comment`
    - `@typescript-eslint/no-confusing-non-null-assertion`
- Turned off rules from Typescript ESlint for its controversity
    - `@typescript-eslint/no-unsafe-assignment`
    - `@typescript-eslint/prefer-readonly-parameter-types`
- Generalize test file patterns to turn off a few rules in tests
- Turned off rules from Typescript ESlint in tests
    - `@typescript-eslint/no-unsafe-assignment`
    - `@typescript-eslint/no-unsafe-return`

### Removed

- Deprecated ESlint native rules (will be added again by usage of another plugin)
- Deprecated Typescript ESlint rules

## [5.0.0] 2020-05-13
### Added
- Readme and change log

### Changed
- Turned on rule for JSX quotes preferring double quotes
- Turned on rule disabling usage of await inside a loop
- Simplified import ordering rules to require 2 groups instead of alphabetized sorting
- Overridden camelcase rule by TypeScript specific rule to support generic type naming convention
- Turned on rules added in updated versions of dependencies
    - `@typescript-eslint/ban-ts-comment`
    - `@typescript-eslint/class-literal-property-style`
    - `@typescript-eslint/comma-spacing`
    - `@typescript-eslint/default-param-last`
    - `@typescript-eslint/dot-notation`
    - `@typescript-eslint/init-declarations`
    - `@typescript-eslint/keyword-spacing`
    - `@typescript-eslint/lines-between-class-members`
    - `@typescript-eslint/method-signature-style`
    - `@typescript-eslint/naming-convention`
    - `@typescript-eslint/no-base-to-string`
    - `@typescript-eslint/no-dupe-class-members`
    - `@typescript-eslint/no-invalid-this`
    - `@typescript-eslint/no-invalid-void-type`
    - `@typescript-eslint/no-non-null-asserted-optional-chain`
    - `@typescript-eslint/no-unnecessary-boolean-literal-compare`
    - `@typescript-eslint/no-unsafe-assignment`
    - `@typescript-eslint/no-unsafe-call`
    - `@typescript-eslint/no-unsafe-member-access`
    - `@typescript-eslint/no-unsafe-return`
    - `@typescript-eslint/prefer-as-const`
    - `@typescript-eslint/prefer-readonly-parameter-types`
    - `@typescript-eslint/prefer-reduce-type-parameter`
    - `@typescript-eslint/prefer-ts-expect-error`
    - `@typescript-eslint/switch-exhaustiveness-check`

### Removed
- Support for plain JS (including Babel parser) and Flow
- Support for Node.js older than 10.13.0

### Fixed
- Invalid definition of optional peer dependencies
