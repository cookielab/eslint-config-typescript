/*
 * temporary workaround while we wait for https://github.com/facebook/jest/issues/9771
 * see https://github.com/typescript-eslint/typescript-eslint/issues/4210
 */
const enhancedResolve = require('enhanced-resolve');

const resolver = enhancedResolve.create.sync({
	conditionNames: ['require', 'node', 'default'],
	extensions: ['.js', '.json'],
	mainFields: ['exports', 'main', 'require'],
});

module.exports = (request, options) => {
	if (['eslint/use-at-your-own-risk'].includes(request)) {
		return resolver(options.basedir, request);
	}

	return options.defaultResolver(request, options);
};
