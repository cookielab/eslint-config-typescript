module.exports = {
	parser: '@typescript-eslint/parser',
	parserOptions: {
		project: './tsconfig.json',
		sourceType: 'module',
	},
	plugins: [
		'@typescript-eslint',
	],
	overrides: [
		{
			files: [
				'*.test.ts',
				'*.test.tsx',
				'*.spec.ts',
				'*.spec.tsx',
				'**/__tests__/*',
			],
			rules: {
				'@typescript-eslint/no-unsafe-assignment': 'off',
				'@typescript-eslint/no-unsafe-return': 'off',
				'@typescript-eslint/unbound-method': 'off',
			},
		},
	],
	rules: {
		'import/exports-last': 'off', // to support exporting types/interfaces anywhere
		'import/group-exports': 'off', // to support exporting types/interfaces anywhere
		'import/no-cycle': 'off', // to support importing types/interfaces
		'no-undef': 'off', // to prevent false positives that are correctly checked by TypeScript itself https://github.com/typescript-eslint/typescript-eslint/issues/2528#issuecomment-689369395
		'@typescript-eslint/adjacent-overload-signatures': 'error',
		'@typescript-eslint/array-type': ['error', {
			default: 'array-simple',
			readonly: 'array-simple',
		}],
		'@typescript-eslint/await-thenable': 'error',
		'@typescript-eslint/ban-ts-comment': ['error', {
			'ts-ignore': true,
			'ts-nocheck': true,
			'ts-check': 'allow-with-description',
			'ts-expect-error': 'allow-with-description',
			'minimumDescriptionLength': 3,
		}],
		'@typescript-eslint/ban-tslint-comment': 'error',
		'@typescript-eslint/ban-types': ['error', {
			types: {
				'{}': {
					message: 'Use Record<never, never> instead',
				},
				'[]': {
					message: '`[]` does only allow empty arrays. Use ReadonlyArray<never> instead',
				},

				/*
				 * The rule for Array is not fully supported yet https://github.com/typescript-eslint/typescript-eslint/commit/c14ade74a61007fa48af9ae4a5535b426080e64d
				 * Array: {
				 * 	message: 'Use Array<any> instead',
				 * },
				 */
			},
			extendDefaults: true,
		}],
		'brace-style': 'off',
		'@typescript-eslint/brace-style': ['error', '1tbs', {
			allowSingleLine: false,
		}],
		'@typescript-eslint/class-literal-property-style': ['error', 'fields'],
		'comma-dangle': 'off',
		'@typescript-eslint/comma-dangle': ['error', {
			arrays: 'always-multiline',
			objects: 'always-multiline',
			imports: 'always-multiline',
			exports: 'always-multiline',
			functions: 'only-multiline', // in future switch to always-multiline
			enums: 'always-multiline',
			generics: 'always-multiline',
			tuples: 'always-multiline',
		}],
		'comma-spacing': 'off',
		'@typescript-eslint/comma-spacing': ['error', {
			before: false,
			after: true,
		}],
		'@typescript-eslint/consistent-indexed-object-style': ['error', 'record'],
		'@typescript-eslint/consistent-type-assertions': ['error', {
			assertionStyle: 'as',
			objectLiteralTypeAssertions: 'never',
		}],
		'@typescript-eslint/consistent-type-definitions': ['error', 'interface'],
		'@typescript-eslint/consistent-type-imports': ['error', {
			prefer: 'no-type-imports',
			disallowTypeAnnotations: true,
		}],
		'@typescript-eslint/consistent-type-exports': ['error', {
			fixMixedExportsWithInlineTypeSpecifier: false,
		}],
		'default-param-last': 'off',
		'@typescript-eslint/default-param-last': 'error',
		'dot-notation': 'off',
		'@typescript-eslint/dot-notation': ['error', {
			allowKeywords: true,
			allowPattern: '',
			allowPrivateClassPropertyAccess: false,
			allowProtectedClassPropertyAccess: false,
			allowIndexSignaturePropertyAccess: true,
		}],
		'@typescript-eslint/explicit-function-return-type': ['error', {
			allowExpressions: true,
			allowTypedFunctionExpressions: true,
			allowHigherOrderFunctions: true,
			allowDirectConstAssertionInArrowFunctions: true,
			allowConciseArrowFunctionExpressionsStartingWithVoid: false,
			allowedNames: [],
		}],
		'@typescript-eslint/explicit-member-accessibility': ['error', {
			accessibility: 'explicit',
			overrides: {
				accessors: 'explicit',
				constructors: 'explicit',
				methods: 'explicit',
				properties: 'explicit',
				parameterProperties: 'explicit',
			},
			ignoredMethodNames: [],
		}],
		'@typescript-eslint/explicit-module-boundary-types': 'off',
		'func-call-spacing': 'off', // Disable base rule to avoid errors
		'@typescript-eslint/func-call-spacing': ['error', 'never'],
		'indent': 'off', // Disable base rule to avoid errors
		'@typescript-eslint/indent': ['error', 'tab', {
			SwitchCase: 1,
			VariableDeclarator: 1,
			outerIIFEBody: 1,
			MemberExpression: 1,
			FunctionDeclaration: {
				parameters: 1,
				body: 1,
			},
			FunctionExpression: {
				parameters: 1,
				body: 1,
			},
			CallExpression: {
				arguments: 1,
			},
			ArrayExpression: 1,
			ObjectExpression: 1,
			ImportDeclaration: 1,
			flatTernaryExpressions: false,
			offsetTernaryExpressions: false,
			ignoredNodes: [],
			ignoreComments: false,
			StaticBlock: {
				body: 1,
			},
		}],
		'init-declarations': 'off',
		'@typescript-eslint/init-declarations': ['error', 'always'],
		'keyword-spacing': 'off',
		'@typescript-eslint/keyword-spacing': ['error', {
			before: true,
			after: true,
			overrides: {
				abstract: {
					before: true,
					after: true,
				},
				boolean: {
					before: true,
					after: true,
				},
				break: {
					before: true,
					after: true,
				},
				byte: {
					before: true,
					after: true,
				},
				case: {
					before: true,
					after: true,
				},
				catch: {
					before: true,
					after: true,
				},
				char: {
					before: true,
					after: true,
				},
				class: {
					before: true,
					after: true,
				},
				const: {
					before: true,
					after: true,
				},
				continue: {
					before: true,
					after: true,
				},
				debugger: {
					before: true,
					after: true,
				},
				default: {
					before: true,
					after: true,
				},
				delete: {
					before: true,
					after: true,
				},
				do: {
					before: true,
					after: true,
				},
				double: {
					before: true,
					after: true,
				},
				else: {
					before: true,
					after: true,
				},
				enum: {
					before: true,
					after: true,
				},
				export: {
					before: true,
					after: true,
				},
				extends: {
					before: true,
					after: true,
				},
				false: {
					before: true,
					after: true,
				},
				final: {
					before: true,
					after: true,
				},
				finally: {
					before: true,
					after: true,
				},
				float: {
					before: true,
					after: true,
				},
				for: {
					before: true,
					after: true,
				},
				function: {
					before: true,
					after: true,
				},
				goto: {
					before: true,
					after: true,
				},
				if: {
					before: true,
					after: true,
				},
				implements: {
					before: true,
					after: true,
				},
				import: {
					before: true,
					after: true,
				},
				in: {
					before: true,
					after: true,
				},
				instanceof: {
					before: true,
					after: true,
				},
				int: {
					before: true,
					after: true,
				},
				interface: {
					before: true,
					after: true,
				},
				long: {
					before: true,
					after: true,
				},
				native: {
					before: true,
					after: true,
				},
				new: {
					before: true,
					after: true,
				},
				null: {
					before: true,
					after: true,
				},
				package: {
					before: true,
					after: true,
				},
				private: {
					before: true,
					after: true,
				},
				protected: {
					before: true,
					after: true,
				},
				public: {
					before: true,
					after: true,
				},
				return: {
					before: true,
					after: true,
				},
				short: {
					before: true,
					after: true,
				},
				static: {
					before: true,
					after: true,
				},
				super: {
					before: true,
					after: true,
				},
				switch: {
					before: true,
					after: true,
				},
				synchronized: {
					before: true,
					after: true,
				},
				this: {
					before: true,
					after: true,
				},
				throw: {
					before: true,
					after: true,
				},
				throws: {
					before: true,
					after: true,
				},
				transient: {
					before: true,
					after: true,
				},
				true: {
					before: true,
					after: true,
				},
				try: {
					before: true,
					after: true,
				},
				typeof: {
					before: true,
					after: true,
				},
				var: {
					before: true,
					after: true,
				},
				void: {
					before: true,
					after: true,
				},
				volatile: {
					before: true,
					after: true,
				},
				while: {
					before: true,
					after: true,
				},
				with: {
					before: true,
					after: true,
				},
				as: {
					before: true,
					after: true,
				},
				async: {
					before: true,
					after: true,
				},
				await: {
					before: true,
					after: true,
				},
				from: {
					before: true,
					after: true,
				},
				get: {
					before: true,
					after: true,
				},
				let: {
					before: true,
					after: true,
				},
				of: {
					before: true,
					after: true,
				},
				set: {
					before: true,
					after: true,
				},
				yield: {
					before: true,
					after: true,
				},
			},
		}],
		'lines-between-class-members': 'off',
		'@typescript-eslint/lines-between-class-members': ['error', 'always', {
			exceptAfterOverload: true,
			exceptAfterSingleLine: true,
		}],
		'@typescript-eslint/member-delimiter-style': ['error', {
			multilineDetection: 'brackets',
			multiline: {
				delimiter: 'semi',
				requireLast: true,
			},
			singleline: {
				delimiter: 'semi',
				requireLast: false,
			},
			overrides: {
				interface: {
					multiline: {
						delimiter: 'semi',
						requireLast: true,
					},
					singleline: {
						delimiter: 'semi',
						requireLast: false,
					},
				},
				typeLiteral: {
					multiline: {
						delimiter: 'comma',
						requireLast: true,
					},
					singleline: {
						delimiter: 'comma',
						requireLast: false,
					},
				},
			},
		}],
		'@typescript-eslint/member-ordering': ['error', {
			default: [
				'signature',
				'public-static-field',
				'protected-static-field',
				'private-static-field',
				'public-instance-field',
				'protected-instance-field',
				'private-instance-field',
				'public-abstract-field',
				'protected-abstract-field',
				'private-abstract-field',
				'public-field',
				'protected-field',
				'private-field',
				'static-field',
				'instance-field',
				'abstract-field',
				'field',
				'constructor',
				'public-static-method',
				'protected-static-method',
				'private-static-method',
				'public-instance-method',
				'protected-instance-method',
				'private-instance-method',
				'public-abstract-method',
				'protected-abstract-method',
				'private-abstract-method',
				'public-method',
				'protected-method',
				'private-method',
				'static-method',
				'instance-method',
				'abstract-method',
				'method',
			],
			classes: [
				'signature',
				'public-static-field',
				'protected-static-field',
				'private-static-field',
				'public-instance-field',
				'protected-instance-field',
				'private-instance-field',
				'public-abstract-field',
				'protected-abstract-field',
				'private-abstract-field',
				'public-field',
				'protected-field',
				'private-field',
				'static-field',
				'instance-field',
				'abstract-field',
				'field',
				'constructor',
				'public-static-method',
				'protected-static-method',
				'private-static-method',
				'public-instance-method',
				'protected-instance-method',
				'private-instance-method',
				'public-abstract-method',
				'protected-abstract-method',
				'private-abstract-method',
				'public-method',
				'protected-method',
				'private-method',
				'static-method',
				'instance-method',
				'abstract-method',
				'method',
			],
			classExpressions: [
				'signature',
				'public-static-field',
				'protected-static-field',
				'private-static-field',
				'public-instance-field',
				'protected-instance-field',
				'private-instance-field',
				'public-abstract-field',
				'protected-abstract-field',
				'private-abstract-field',
				'public-field',
				'protected-field',
				'private-field',
				'static-field',
				'instance-field',
				'abstract-field',
				'field',
				'constructor',
				'public-static-method',
				'protected-static-method',
				'private-static-method',
				'public-instance-method',
				'protected-instance-method',
				'private-instance-method',
				'public-abstract-method',
				'protected-abstract-method',
				'private-abstract-method',
				'public-method',
				'protected-method',
				'private-method',
				'static-method',
				'instance-method',
				'abstract-method',
				'method',
			],
			interfaces: ['signature', 'field', 'constructor', 'method'],
			typeLiterals: ['signature', 'field', 'constructor', 'method'],
		}],
		'@typescript-eslint/method-signature-style': ['error', 'property'],
		'@typescript-eslint/naming-convention': [
			'error',
			{
				selector: 'default',
				format: ['camelCase'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'variable',
				format: ['camelCase', 'UPPER_CASE'],
				modifiers: ['global'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'property',
				format: ['camelCase', 'PascalCase', 'snake_case', 'UPPER_CASE'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'variable',
				format: ['camelCase'],
				types: ['function'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'enumMember',
				format: ['UPPER_CASE'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'typeLike',
				format: ['PascalCase'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			// https://typescript-eslint.io/rules/naming-convention#ignore-properties-that-require-quotes
			{
				selector: 'objectLiteralProperty',
				format: null,
				modifiers: ['requiresQuotes'],
			},
		],
		'no-array-constructor': 'off',
		'@typescript-eslint/no-array-constructor': 'error',
		'@typescript-eslint/no-base-to-string': ['error', {
			ignoredTypeNames: ['RegExp'],
		}],
		'@typescript-eslint/no-confusing-non-null-assertion': 'error',
		'@typescript-eslint/no-confusing-void-expression': ['error', {
			ignoreArrowShorthand: false,
			ignoreVoidOperator: false,
		}],
		'no-dupe-class-members': 'off',
		'@typescript-eslint/no-dupe-class-members': 'error',
		'no-duplicate-imports': 'off',
		'@typescript-eslint/no-duplicate-imports': ['error', {
			includeExports: false,
		}],
		'@typescript-eslint/no-dynamic-delete': 'error',
		'no-empty-function': 'off',
		'@typescript-eslint/no-empty-function': ['error', {
			allow: [
				'private-constructors',
				'protected-constructors',
			],
		}],
		'@typescript-eslint/no-empty-interface': ['error', {
			allowSingleExtends: true,
		}],
		'@typescript-eslint/no-explicit-any': ['error', {
			fixToUnknown: true,
			ignoreRestArgs: false,
		}],
		'@typescript-eslint/no-extra-non-null-assertion': 'error',
		'@typescript-eslint/no-extra-parens': 'off',
		'no-extra-semi': 'off',
		'@typescript-eslint/no-extra-semi': 'error',
		'@typescript-eslint/no-extraneous-class': ['error', {
			allowConstructorOnly: false,
			allowEmpty: false,
			allowStaticOnly: false,
			allowWithDecorator: false,
		}],
		'@typescript-eslint/no-floating-promises': ['error', {
			ignoreVoid: false,
			ignoreIIFE: false,
		}],
		'@typescript-eslint/no-for-in-array': 'error',
		'@typescript-eslint/no-implicit-any-catch': ['error', {
			allowExplicitAny: false,
		}],
		'@typescript-eslint/no-implied-eval': 'error',
		'@typescript-eslint/no-inferrable-types': 'off',
		'no-invalid-this': 'off',
		'@typescript-eslint/no-invalid-this': ['error', {
			capIsConstructor: true,
		}],
		'@typescript-eslint/no-invalid-void-type': ['error', {
			allowInGenericTypeArguments: true,
			allowAsThisParameter: false,
		}],
		'no-loop-func': 'off',
		'@typescript-eslint/no-loop-func': 'error',
		'no-loss-of-precision': 'off',
		'@typescript-eslint/no-loss-of-precision': 'error',
		'@typescript-eslint/no-magic-numbers': 'off',
		'@typescript-eslint/no-meaningless-void-operator': ['error', {
			checkNever: true,
		}],
		'@typescript-eslint/no-misused-new': 'error',
		'@typescript-eslint/no-misused-promises': ['error', {
			checksConditionals: true,
			checksVoidReturn: true,
		}],
		'@typescript-eslint/no-namespace': ['error', {
			allowDeclarations: false,
			allowDefinitionFiles: true,
		}],
		'@typescript-eslint/no-non-null-asserted-nullish-coalescing': 'error',
		'@typescript-eslint/no-non-null-assertion': 'error',
		'@typescript-eslint/no-non-null-asserted-optional-chain': 'error',
		'@typescript-eslint/no-parameter-properties': 'off',
		'no-redeclare': 'off',
		'@typescript-eslint/no-redeclare': ['error', {
			builtinGlobals: true,
			ignoreDeclarationMerge: true,
		}],
		'@typescript-eslint/no-redundant-type-constituents': 'error',
		'@typescript-eslint/no-require-imports': 'error',
		'no-shadow': 'off',
		'@typescript-eslint/no-shadow': ['error', {
			builtinGlobals: false,
			hoist: 'functions',
			allow: [],
			ignoreTypeValueShadow: false,
			ignoreFunctionTypeParameterNameValueShadow: false,
		}],
		'@typescript-eslint/no-restricted-imports': 'off',
		'@typescript-eslint/no-throw-literal': ['error', {
			allowThrowingAny: false,
			allowThrowingUnknown: true,
		}],
		'no-unused-expressions': 'off',
		'@typescript-eslint/no-unused-expressions': ['error', {
			allowShortCircuit: false,
			allowTernary: false,
			allowTaggedTemplates: false,
			enforceForJSX: true,
		}],
		'no-unused-vars': 'off',
		'@typescript-eslint/no-unused-vars': ['error', {
			vars: 'all',
			varsIgnorePattern: 'a^',
			args: 'after-used',
			ignoreRestSiblings: false,
			argsIgnorePattern: 'a^',
			caughtErrors: 'none',
			caughtErrorsIgnorePattern: 'a^',
		}],
		'@typescript-eslint/no-this-alias': ['error', {
			allowDestructuring: true,
			allowedNames: [],
		}],
		'@typescript-eslint/no-unnecessary-boolean-literal-compare': ['error', {
			allowComparingNullableBooleansToTrue: true,
			allowComparingNullableBooleansToFalse: true,
		}],
		'@typescript-eslint/no-unnecessary-condition': ['error', {
			allowConstantLoopConditions: false,
			allowRuleToRunWithoutStrictNullChecksIKnowWhatIAmDoing: false,
		}],
		'@typescript-eslint/no-type-alias': ['error', {
			allowAliases: 'in-unions-and-intersections',
			allowCallbacks: 'always',
			allowConditionalTypes: 'always',
			allowConstructors: 'never',
			allowGenerics: 'always',
			allowLiterals: 'always',
			allowMappedTypes: 'always',
			allowTupleTypes: 'always',
		}],
		'@typescript-eslint/no-unnecessary-qualifier': 'error',
		'@typescript-eslint/no-unnecessary-type-arguments': 'error',
		'@typescript-eslint/no-unnecessary-type-assertion': ['error', {
			typesToIgnore: [],
		}],
		'@typescript-eslint/no-unnecessary-type-constraint': 'error',
		'@typescript-eslint/no-unsafe-assignment': 'off', // Enable once @cookielab.io/postgres-client methods allow custom return types through generics.
		'@typescript-eslint/no-unsafe-call': 'error',
		'@typescript-eslint/no-unsafe-member-access': 'error',
		'@typescript-eslint/no-unsafe-return': 'error',
		'@typescript-eslint/no-unsafe-argument': 'error',
		'no-use-before-define': 'off',
		'@typescript-eslint/no-use-before-define': ['error', {
			functions: true,
			classes: true,
			variables: true,
			enums: true,
			typedefs: true,
			ignoreTypeReferences: false,
		}], // need to be tested with eslint no-use-before-define
		'no-useless-constructor': 'off',
		'@typescript-eslint/no-useless-constructor': 'error',
		'@typescript-eslint/no-useless-empty-export': 'error',
		'@typescript-eslint/no-var-requires': 'error',
		'@typescript-eslint/non-nullable-type-assertion-style': 'error',
		'object-curly-spacing': 'off',
		'@typescript-eslint/object-curly-spacing': ['error', 'never', {
			arraysInObjects: false,
			objectsInObjects: false,
		}],
		'padding-line-between-statements': 'off',
		'@typescript-eslint/padding-line-between-statements': [
			'error',
			{blankLine: 'always', prev: '*', next: 'return'},
			{blankLine: 'always', prev: '*', next: 'case'},
			{blankLine: 'always', prev: '*', next: 'class'},
			{blankLine: 'always', prev: 'class', next: '*'},
			{blankLine: 'always', prev: '*', next: 'default'},
			{blankLine: 'always', prev: '*', next: 'export'},
			{blankLine: 'any', prev: 'export', next: 'export'},
			{blankLine: 'always', prev: 'import', next: '*'},
			{blankLine: 'any', prev: 'import', next: 'import'},
		],
		'@typescript-eslint/prefer-as-const': 'error',
		'@typescript-eslint/prefer-enum-initializers': 'error',
		'@typescript-eslint/prefer-for-of': 'error',
		'@typescript-eslint/prefer-function-type': 'off',
		'@typescript-eslint/prefer-includes': 'error',
		'@typescript-eslint/prefer-literal-enum-member': ['error', {
			allowBitwiseExpressions: true,
		}],
		'@typescript-eslint/prefer-namespace-keyword': 'off',
		'@typescript-eslint/prefer-nullish-coalescing': ['error', {
			ignoreConditionalTests: true,
			ignoreMixedLogicalExpressions: true,
			forceSuggestionFixer: true,
		}],
		'@typescript-eslint/prefer-optional-chain': 'error',
		'@typescript-eslint/prefer-readonly': ['error', {
			onlyInlineLambdas: false,
		}],
		'@typescript-eslint/prefer-readonly-parameter-types': 'off', // Enable once methods and ReadonlySet/Map are considered immutable.
		'@typescript-eslint/prefer-reduce-type-parameter': 'error',
		'@typescript-eslint/prefer-regexp-exec': 'error',
		'@typescript-eslint/prefer-return-this-type': 'error',
		'@typescript-eslint/prefer-string-starts-ends-with': 'error',
		'@typescript-eslint/prefer-ts-expect-error': 'error',
		'@typescript-eslint/promise-function-async': 'off',
		'quotes': 'off',
		'@typescript-eslint/quotes': ['error', 'single', {
			avoidEscape: true,
			allowTemplateLiterals: true,
		}],
		'@typescript-eslint/require-array-sort-compare': 'off',
		'require-await': 'off',
		'@typescript-eslint/require-await': 'error',
		'@typescript-eslint/restrict-plus-operands': ['error', {
			checkCompoundAssignments: false,
			allowAny: false,
		}],
		'@typescript-eslint/restrict-template-expressions': ['error', {
			allowNumber: true,
			allowBoolean: false,
			allowAny: false,
			allowNullish: false,
			allowRegExp: false,
		}],
		'@typescript-eslint/return-await': ['error', 'always'],
		'semi': 'off', // Disable base rule to avoid errors
		'@typescript-eslint/semi': ['error', 'always', {
			omitLastInOneLineBlock: false,
		}],
		'@typescript-eslint/sort-type-union-intersection-members': ['error', {
			checkIntersections: true,
			checkUnions: true,
			groupOrder: [
				'named',
				'keyword',
				'operator',
				'literal',
				'function',
				'import',
				'conditional',
				'object',
				'tuple',
				'intersection',
				'union',
				'nullish',
			],
		}],
		'space-before-function-paren': 'off',
		'space-before-blocks': 'off',
		'@typescript-eslint/space-before-blocks': ['error', 'always'],
		'@typescript-eslint/space-before-function-paren': ['error', {
			anonymous: 'always',
			named: 'never',
			asyncArrow: 'always',
		}],
		'space-infix-ops': 'off',
		'@typescript-eslint/space-infix-ops': ['error', {
			int32Hint: false,
		}],
		'@typescript-eslint/strict-boolean-expressions': ['error', {
			allowString: false,
			allowNumber: false,
			allowNullableObject: false,
			allowNullableBoolean: false,
			allowNullableString: false,
			allowNullableNumber: false,
			allowAny: false,
			allowRuleToRunWithoutStrictNullChecksIKnowWhatIAmDoing: false,
		}],
		'@typescript-eslint/switch-exhaustiveness-check': 'error',
		'@typescript-eslint/triple-slash-reference': ['error', {
			path: 'never',
			types: 'never',
			lib: 'never',
		}],
		'@typescript-eslint/type-annotation-spacing': ['error', {
			before: false,
			after: true,
			overrides: {
				colon: {
					before: false,
					after: true,
				},
				arrow: {
					before: true,
					after: true,
				},
				variable: {
					before: false,
					after: true,
				},
				parameter: {
					before: false,
					after: true,
				},
				property: {
					before: false,
					after: true,
				},
				returnType: {
					before: false,
					after: true,
				},
			},
		}],
		'@typescript-eslint/typedef': ['error', {
			arrayDestructuring: false,
			arrowParameter: true,
			memberVariableDeclaration: true,
			objectDestructuring: false,
			parameter: true,
			propertyDeclaration: true,
			variableDeclaration: false,
			variableDeclarationIgnoreFunction: true,
		}],
		'@typescript-eslint/unbound-method': ['error', {
			ignoreStatic: false,
		}],
		'@typescript-eslint/unified-signatures': 'error',
	},
};
